package io.gitlab.lulnope.hotswap.mojo;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import io.gitlab.lulnope.hotswap.model.WorkspaceEntry;
import io.gitlab.lulnope.hotswap.model.filters.AbstractFilter;
import io.gitlab.lulnope.hotswap.model.filters.FilterGroup;
import lombok.NonNull;

public class FilterExecutor {
	private final Map<String, List<AbstractFilter>> filtersPerGroupId;
	
	
	FilterExecutor(Collection<FilterGroup> filterGroups) {
		this.filtersPerGroupId = Collections.unmodifiableMap(filterGroups.stream()
				.collect(toMap(FilterGroup::getId, FilterGroup::getFilters)));
	}
	
	
	public boolean passesFilters(@NonNull WorkspaceEntry entry) {
		return entry.getFilterGroupIds().stream()
			.map(filtersPerGroupId::get)
			.filter(Objects::nonNull)
			.flatMap(Collection::stream)
			.allMatch(e -> e.test(entry));
	}
	
	public List<WorkspaceEntry> filter(@NonNull Collection<WorkspaceEntry> entries) {
		return entries.stream()
				.filter(this::passesFilters)
				.collect(toList());
	}
}
