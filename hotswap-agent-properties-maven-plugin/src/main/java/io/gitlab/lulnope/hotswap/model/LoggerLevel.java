package io.gitlab.lulnope.hotswap.model;


/**
 * Allowed values:
 * <li>{@link #ERROR}</li>
 * <li>{@link #RELOAD}</li>
 * <li>{@link #WARNING}</li>
 * <li>{@link #INFO}</li>
 * <li>{@link #DEBUG}</li>
 * <li>{@link #TRACE}</li>
 * 
 * @since 0.0.1
 */
public enum LoggerLevel {
    ERROR,
    RELOAD,
    WARNING,
    INFO,
    DEBUG,
    TRACE;
	
	@Override
	public String toString() {
		return name().toLowerCase();
	}
}
