package io.gitlab.lulnope.hotswap.model.filters;

import static java.util.Optional.ofNullable;

import org.apache.maven.model.Model;

import io.gitlab.lulnope.hotswap.model.WorkspaceEntry;
import lombok.ToString;

@ToString(callSuper=true)
public class PackagingFilter extends AbstractProjectFilter {

	@Override
	String extractValue(WorkspaceEntry entry) {
		Model model = entry.getModel();
		return ofNullable(model.getPackaging()).orElse("jar");
	}

}
