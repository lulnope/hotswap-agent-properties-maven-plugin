package io.gitlab.lulnope.hotswap.model.filters;

import static java.util.Optional.ofNullable;

import org.apache.maven.model.Model;
import org.apache.maven.model.Parent;

import io.gitlab.lulnope.hotswap.model.WorkspaceEntry;
import lombok.ToString;

@ToString(callSuper=true)
public class VersionFilter extends AbstractProjectFilter {

	@Override
	String extractValue(WorkspaceEntry entry) {
		Model model = entry.getModel();
		String version = model.getVersion();
		String parentVersion = ofNullable(model.getParent())
			.map(Parent::getVersion)
			.orElse(null);
		
		return ofNullable(version).orElse(parentVersion);
	}

}
