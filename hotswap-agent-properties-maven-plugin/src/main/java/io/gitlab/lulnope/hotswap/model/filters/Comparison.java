package io.gitlab.lulnope.hotswap.model.filters;

import java.util.function.BiPredicate;

import lombok.AllArgsConstructor;

/**
 * Specifies comparison operator.
 * 
 * Allowed values:
 * <li>{@link #EQUALS}</li>
 * <li>{@link #STARTS_WITH}</li>
 * <li>{@link #ENDS_WITH}</li>
 * <li>{@link #CONTAINS}</li>
 * <li>{@link #REGEXP}</li>
 * @since 0.0.1
 */
@AllArgsConstructor
public enum Comparison implements BiPredicate<String, String>{
	EQUALS{
		@Override
		public boolean test(String filter, String value) {
			return value.equals(filter);
		}
	},
	
	STARTS_WITH{
		@Override
		public boolean test(String filter, String value) {
			return value.startsWith(filter);
		}
	},
	
	ENDS_WITH {
		@Override
		public boolean test(String filter, String value) {
			return value.endsWith(filter);
		}
	},
	
	CONTAINS {
		@Override
		public boolean test(String filter, String value) {
			return value.contains(filter);
		}
	},
	
	REGEXP {
		@Override
		public boolean test(String filter, String value) {
			return value.matches(filter);
		}
	};
}
