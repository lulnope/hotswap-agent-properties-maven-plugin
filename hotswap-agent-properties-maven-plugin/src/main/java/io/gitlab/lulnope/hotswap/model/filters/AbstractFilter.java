package io.gitlab.lulnope.hotswap.model.filters;

import java.util.function.Predicate;

import org.apache.maven.plugins.annotations.Parameter;

import io.gitlab.lulnope.hotswap.model.WorkspaceEntry;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.ToString;

@ToString
public abstract class AbstractFilter implements Predicate<WorkspaceEntry>{
	/**
	 * If set to true - negates the result of {@link comparison} operation.
	 * Otherwise does nothing.
	 * @since 0.0.1
	 */
	@Parameter(defaultValue = "false")
	@Getter(value=AccessLevel.PROTECTED)
	private boolean negate = false;
}
