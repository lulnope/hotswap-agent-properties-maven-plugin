package io.gitlab.lulnope.hotswap.model;

/**
 * Allowed values:
 * <li>{@link #CLASS_CHANGE}</li>
 * <li>{@link #METHOD_FIELD_SIGNATURE_CHANGE}</li>
 * <li>{@link #FIELD_SIGNATURE_CHANGE}</li>
 * <li>{@link #NEVER}</li>
 * 
 * @since 0.0.1
 */
public enum WeldBeanReloadStrategy {
	/**
	 * reload bean instance on any class modification, plus reaload on changes specified in
	 * METHOD_FIELD_SIGNATURE_CHANGE and FIELD_SIGNATURE_CHANGE strategies
	 */
	CLASS_CHANGE,

	/**
	 * reload bean instance on any method/field change. Includes changes specified in
	 * strategy FIELD_SIGNATURE_CHANGE
	 */
	METHOD_FIELD_SIGNATURE_CHANGE,
	
	/**
	 * reload bean instance on any field signature change. Includes also field annotation changes
	 */
	FIELD_SIGNATURE_CHANGE,
	
	/**
	 * never reload bean (default)
	 */
	NEVER;
}
