package io.gitlab.lulnope.hotswap.model.filters;

import io.gitlab.lulnope.hotswap.model.WorkspaceEntry;
import lombok.ToString;

@ToString(callSuper=true)
public class ArtifactIdFilter extends AbstractProjectFilter {

	@Override
	String extractValue(WorkspaceEntry e) {
		return e.getId().getArtifactId();
	}

}
