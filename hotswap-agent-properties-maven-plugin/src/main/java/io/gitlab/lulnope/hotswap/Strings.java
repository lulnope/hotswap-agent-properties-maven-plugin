package io.gitlab.lulnope.hotswap;


public class Strings {
	public static boolean isNullOrEmpty(String s) {
		return s == null || s.isEmpty();
	}
	
	public static boolean isNeitherNullNorEmpty(String s) {
		return !isNullOrEmpty(s);
	}
}
