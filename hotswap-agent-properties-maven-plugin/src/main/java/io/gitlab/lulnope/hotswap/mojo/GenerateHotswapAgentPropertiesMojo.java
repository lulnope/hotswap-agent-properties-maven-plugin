package io.gitlab.lulnope.hotswap.mojo;

import io.gitlab.lulnope.hotswap.model.HotswapAgentProperties;
import io.gitlab.lulnope.hotswap.model.LoggerLevel;
import io.gitlab.lulnope.hotswap.model.WeldBeanReloadStrategy;
import io.gitlab.lulnope.hotswap.model.Workspace;
import io.gitlab.lulnope.hotswap.model.WorkspaceEntry;
import io.gitlab.lulnope.hotswap.model.filters.FilterGroup;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.DefaultProjectBuildingRequest;
import org.apache.maven.project.MavenProject;
import org.apache.maven.project.ProjectBuildingRequest;
import org.apache.maven.shared.dependency.graph.DependencyGraphBuilder;
import org.apache.maven.shared.dependency.graph.DependencyGraphBuilderException;
import org.apache.maven.shared.dependency.graph.DependencyNode;
import org.apache.maven.shared.dependency.graph.traversal.CollectingDependencyNodeVisitor;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static java.util.stream.Collectors.toList;

@Mojo(name = GenerateHotswapAgentPropertiesMojo.GOAL,
        requiresDirectInvocation = false,
        defaultPhase = LifecyclePhase.PROCESS_RESOURCES,
        requiresDependencyCollection = ResolutionScope.COMPILE_PLUS_RUNTIME,
        requiresDependencyResolution = ResolutionScope.COMPILE_PLUS_RUNTIME,
        requiresOnline = false,
        threadSafe = true)
public class GenerateHotswapAgentPropertiesMojo extends AbstractMojo {

    static final String GOAL = "generate";

    @Parameter(defaultValue = "${project}", readonly = true)
    private MavenProject project;

    @Parameter(defaultValue = "${session}", readonly = true, required = true)
    private MavenSession session;

    @Parameter(defaultValue = "${reactorProjects}", readonly = true, required = true)
    private List<MavenProject> reactorProjects;

    @Component(hint = "default")
    private DependencyGraphBuilder dependencyGraphBuilder;

    /**
     * Filter definitions to be used in workspace element.
     *
     * @see FilterGroup
     */
    @Parameter
    private List<FilterGroup> filterGroups = new ArrayList<>();

    /**
     * Workspace definitions pointing to actual file system paths
     * to be scanned for Maven projects. Only the projects passing all referenced
     * filters will be included in resulting hotswap-agent.properties file. Every directory containing
     * pom.xml file will be treated as a project's base directory and it's
     * build directory and resources directories will be added to extraClasspath and
     * watchResources respectively. By default only projects referenced in dependencies are included.
     * <p>
     * Works for webappDir as well.
     *
     * @see Workspace
     */
    @Parameter(required = true)
    private List<Workspace> workspaces = new ArrayList<>();

    /**
     * extraClasspaths as defined in
     * <a href=https://github.com/HotswapProjects/HotswapAgent/blob/master/hotswap-agent-core/src/main/resources/hotswap-agent.properties>
     * https://github.com/HotswapProjects/HotswapAgent/blob/master/hotswap-agent-core/src/main/resources/hotswap-agent.properties</a>
     */
    @Parameter(required = false)
    private List<File> extraClasspaths;

    /**
     * watchResources as defined in
     * <a href=https://github.com/HotswapProjects/HotswapAgent/blob/master/hotswap-agent-core/src/main/resources/hotswap-agent.properties>
     * https://github.com/HotswapProjects/HotswapAgent/blob/master/hotswap-agent-core/src/main/resources/hotswap-agent.properties</a>
     */
    @Parameter(required = false)
    private List<File> watchResourceDirs;

    /**
     * webappDir as defined in
     * <a href=https://github.com/HotswapProjects/HotswapAgent/blob/master/hotswap-agent-core/src/main/resources/hotswap-agent.properties>
     * https://github.com/HotswapProjects/HotswapAgent/blob/master/hotswap-agent-core/src/main/resources/hotswap-agent.properties</a>
     */
    @Parameter(required = false)
    private List<File> webappDirs;

    /**
     * disabledPlugins as defined in
     * <a href=https://github.com/HotswapProjects/HotswapAgent/blob/master/hotswap-agent-core/src/main/resources/hotswap-agent.properties>
     * https://github.com/HotswapProjects/HotswapAgent/blob/master/hotswap-agent-core/src/main/resources/hotswap-agent.properties</a>
     */
    @Parameter
    private String disabledPlugins;

    /**
     * spring.basePackagePrefix as defined in
     * <a href=https://github.com/HotswapProjects/HotswapAgent/blob/master/hotswap-agent-core/src/main/resources/hotswap-agent.properties>
     * https://github.com/HotswapProjects/HotswapAgent/blob/master/hotswap-agent-core/src/main/resources/hotswap-agent.properties</a>
     */
    @Parameter
    private String springBasePackagePrefix;

    /**
     * autoHotswap as defined in
     * <a href=https://github.com/HotswapProjects/HotswapAgent/blob/master/hotswap-agent-core/src/main/resources/hotswap-agent.properties>
     * https://github.com/HotswapProjects/HotswapAgent/blob/master/hotswap-agent-core/src/main/resources/hotswap-agent.properties</a>
     */
    @Parameter(defaultValue = "false")
    private boolean autoHotswap = false;

    /**
     * autoHotswap.port as defined in
     * <a href=https://github.com/HotswapProjects/HotswapAgent/blob/master/hotswap-agent-core/src/main/resources/hotswap-agent.properties>
     * https://github.com/HotswapProjects/HotswapAgent/blob/master/hotswap-agent-core/src/main/resources/hotswap-agent.properties</a>
     */
    @Parameter
    private Integer autoHotswapPort;

    /**
     * osgiEquinox.debugMod as defined in
     * <a href=https://github.com/HotswapProjects/HotswapAgent/blob/master/hotswap-agent-core/src/main/resources/hotswap-agent.properties>
     * https://github.com/HotswapProjects/HotswapAgent/blob/master/hotswap-agent-core/src/main/resources/hotswap-agent.properties</a>
     */
    @Parameter(defaultValue = "false")
    private boolean osgiEquinoxDebugMode;

    /**
     * weld.beanReloadStrategy as defined in
     * <a href=https://github.com/HotswapProjects/HotswapAgent/blob/master/hotswap-agent-core/src/main/resources/hotswap-agent.properties>
     * https://github.com/HotswapProjects/HotswapAgent/blob/master/hotswap-agent-core/src/main/resources/hotswap-agent.properties</a>
     */
    @Parameter(defaultValue = "CLASS_CHANGE")
    private WeldBeanReloadStrategy weldBeanReloadStrategy = WeldBeanReloadStrategy.CLASS_CHANGE;

    /**
     * Only root logger settings is available through this plugin. </br></br>
     * LOGGER as defined in
     * <a href=https://github.com/HotswapProjects/HotswapAgent/blob/master/hotswap-agent-core/src/main/resources/hotswap-agent.properties>
     * https://github.com/HotswapProjects/HotswapAgent/blob/master/hotswap-agent-core/src/main/resources/hotswap-agent.properties</a>
     * </br>
     * </br>
     * Allowed values are:
     * <li>ERROR</li>
     * <li>RELOAD</li>
     * <li>WARNING</li>
     * <li>INFO</li>
     * <li>DEBUG</li>
     * <li>TRACE</li>
     *
     * @see LoggerLevel
     */
    @Parameter(defaultValue = "INFO", required = true)
    private LoggerLevel logger;

    /**
     * LOGGER_DATETIME_FORMAT as defined in
     * <a href=https://github.com/HotswapProjects/HotswapAgent/blob/master/hotswap-agent-core/src/main/resources/hotswap-agent.properties>
     * https://github.com/HotswapProjects/HotswapAgent/blob/master/hotswap-agent-core/src/main/resources/hotswap-agent.properties</a>
     */
    @Parameter(defaultValue = "HH:mm:ss.SSS")
    private String loggerDateTimeFormat = "HH:mm:ss.SSS";


    /**
     * excludedClassLoaderPatterns as defined in
     * <a href=https://github.com/HotswapProjects/HotswapAgent/blob/master/hotswap-agent-core/src/main/resources/hotswap-agent.properties>
     * https://github.com/HotswapProjects/HotswapAgent/blob/master/hotswap-agent-core/src/main/resources/hotswap-agent.properties</a>
     */
    @Parameter
    private String excludedClassLoaderPatterns;

    /**
     * This project's build output directory - it's absolute path will be
     * copied to resulting hotswap-agent.properties into extraClasspath field.
     */
    @Parameter(defaultValue = "${project.build.outputDirectory}", readonly = true)
    private File buildOutputDirectory;

    /**
     * This project's resources directory - it's absolute path will be
     * copied to resulting hotswap-agent.properties into watchResources field.
     */
    @Parameter(defaultValue = "src/main/resources")
    private File resourcesSubdirectory = new File("src/main/resources");

    /**
     * This project's webapp directory - it's absolute path will be
     * copied to resulting hotswap-agent.properties into webappDir field.
     */
    @Parameter(defaultValue = "src/main/webapp")
    private File webappSubdirectory = new File("src/main/webapp");

    /*
     * If set to true skips plugin execution
     */
    @Parameter(defaultValue = "false")
    private boolean skip = false;

    /*
     * disabled generation of watchResources key
     */
    @Parameter(defaultValue = "false")
    private boolean disableWatchResources = false;

    /*
     * disabled generation of webappDir key
     */
    @Parameter(defaultValue = "false")
    private boolean disableWebappDir = false;

    /*
     * If set to true excludes own sources and resources directories from resulting hotswap-agent.properties
     */
    @Parameter(defaultValue = "false")
    private boolean excludeSelf = false;

    private void validateParameters() throws MojoFailureException {
        if (workspaces.isEmpty()) {
            throw new MojoFailureException("No workspace specified");
        }

        if (buildOutputDirectory == null) {
            throw new MojoFailureException("No build output directory specified");
        }

        if (!buildOutputDirectory.isDirectory()) {
            throw new MojoFailureException("Build output path " + buildOutputDirectory + " is not a directory");
        }
    }

    public void execute() throws MojoExecutionException, MojoFailureException {
        if (skip) {
            getLog().info("Skipping execution");
        }

        validateParameters();

        FilterExecutor fe = new FilterExecutor(filterGroups);
        List<WorkspaceEntry> entries = getWorkspaceEntries();
        List<WorkspaceEntry> filteredEntries = fe.filter(entries);

        List<Path> buildOutputPaths = filteredEntries.stream()
                .filter(WorkspaceEntry::buildOutputDirectoryExists)
                .map(WorkspaceEntry::getBuildOutputRoot)
                .distinct()
                .collect(toList());

        List<Path> resourcePaths = filteredEntries.stream()
                .filter(WorkspaceEntry::resourcesDirectoryExists)
                .map(WorkspaceEntry::getResourcesRoot)
                .distinct()
                .collect(toList());

        List<Path> webappPaths = filteredEntries.stream()
                .filter(WorkspaceEntry::webappDirectoryExists)
                .map(WorkspaceEntry::getWebappRoot)
                .distinct()
                .collect(toList());

        if (extraClasspaths != null) {
            extraClasspaths.stream()
                    .map(e -> Paths.get(e.getAbsolutePath()))
                    .forEach(buildOutputPaths::add);
        }

        if (watchResourceDirs != null) {
            watchResourceDirs.stream()
                    .map(e -> Paths.get(e.getAbsolutePath()))
                    .forEach(resourcePaths::add);
        }

        if (webappDirs != null) {
            webappDirs.stream()
                    .map(e -> Paths.get(e.getAbsolutePath()))
                    .forEach(webappPaths::add);
        }

        Path projectOutputPath = Paths.get(buildOutputDirectory.getAbsolutePath());
        Path projectResourcesPath = Paths.get(resourcesSubdirectory.getAbsolutePath());
        Path projectWebappPath = Paths.get(webappSubdirectory.getAbsolutePath());
        if (excludeSelf) {
            buildOutputPaths.remove(projectOutputPath);
            resourcePaths.remove(projectResourcesPath);
            webappPaths.remove(projectWebappPath);
        }

        if (disableWatchResources) {
            getLog().info("Discarding all resource directories");
            resourcePaths.clear();
        }

        if (disableWebappDir) {
            getLog().info("Discarding all webapp directories");
            webappPaths.clear();
        }

        buildOutputPaths.forEach(e -> getLog().info("Adding to extraClasspath: " + e));
        resourcePaths.forEach(e -> getLog().info("Adding to watchResources: " + e));
        webappPaths.forEach(e -> getLog().info("Adding to webappDir: " + e));

        HotswapAgentProperties hotswapAgentProperties = HotswapAgentProperties.builder()
                .extraClasspath(buildOutputPaths)
                .watchResources(resourcePaths)
                .webappDir(webappPaths)
                .disabledPlugins(disabledPlugins)
                .autoHotswap(autoHotswap)
                .springBasePackagePrefix(springBasePackagePrefix)
                .autoHotswapPort(autoHotswapPort)
                .osgiEquinoxDebugMode(osgiEquinoxDebugMode)
                .weldBeanReloadStrategy(weldBeanReloadStrategy)
                .logger(logger)
                .loggerDateTimeFormat(loggerDateTimeFormat)
                .excludedClassLoaderPatterns(excludedClassLoaderPatterns)
                .build();

        String hotswapAgentPropertiesFile = hotswapAgentProperties.toString();
        getLog().info("hotswap-agent.properties : " + hotswapAgentPropertiesFile);
        byte[] bytes = hotswapAgentPropertiesFile.getBytes(StandardCharsets.UTF_8);
        Path destination = projectOutputPath.resolve("hotswap-agent.properties");
        try {
            File projectOutputDir = projectOutputPath.toFile();
            if (!projectOutputDir.exists() && !projectOutputDir.mkdirs()) {
                throw new MojoFailureException("Destination folder doesn't exist and cannot be created: " + destination);
            }
            Files.write(destination, bytes);
        } catch (IOException e) {
            throw new MojoFailureException("Failed writing hotswap-agent.properties content to file: " + destination, e);
        }
    }

    private List<WorkspaceEntry> getWorkspaceEntries() throws MojoFailureException {
        WorkspaceEntryExtractor wee = new WorkspaceEntryExtractor(workspaces);
        return wee.findMatchingWorkspaceEntries(getDependencies());
    }

    private List<DependencyNode> getDependencies() throws MojoFailureException {
        try {
            ProjectBuildingRequest buildingRequest = new DefaultProjectBuildingRequest(session.getProjectBuildingRequest());
            buildingRequest.setProject(project);
            DependencyNode rootNode = dependencyGraphBuilder.buildDependencyGraph(buildingRequest, null, reactorProjects);
            CollectingDependencyNodeVisitor collectingVisitor = new CollectingDependencyNodeVisitor();
            rootNode.accept(collectingVisitor);
            return collectingVisitor.getNodes();
        } catch (DependencyGraphBuilderException e) {
            throw new MojoFailureException("Failed to collect dependencies", e);
        }
    }
}
