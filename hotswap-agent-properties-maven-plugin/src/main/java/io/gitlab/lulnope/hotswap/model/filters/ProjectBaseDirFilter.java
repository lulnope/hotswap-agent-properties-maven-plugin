package io.gitlab.lulnope.hotswap.model.filters;

import io.gitlab.lulnope.hotswap.model.WorkspaceEntry;
import lombok.ToString;

@ToString(callSuper=true)
public class ProjectBaseDirFilter extends AbstractProjectFilter {

	@Override
	String extractValue(WorkspaceEntry e) {
		return e.getProjectRoot().toAbsolutePath().toString().replaceAll("\\", "/");
	}

}
