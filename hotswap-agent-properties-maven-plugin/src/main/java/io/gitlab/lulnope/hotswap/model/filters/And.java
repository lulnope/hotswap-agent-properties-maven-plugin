package io.gitlab.lulnope.hotswap.model.filters;

import java.util.Collection;

import io.gitlab.lulnope.hotswap.model.WorkspaceEntry;
import lombok.ToString;

@ToString(callSuper=true)
public class And extends NestedFilter {
	
	@Override
	protected boolean test(Collection<AbstractFilter> filters, WorkspaceEntry entry) {
		return filters.stream()
				.allMatch(e -> e.test(entry));
	}
}
