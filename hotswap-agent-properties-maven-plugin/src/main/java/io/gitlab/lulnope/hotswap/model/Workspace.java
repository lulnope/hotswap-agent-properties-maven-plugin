package io.gitlab.lulnope.hotswap.model;

import io.gitlab.lulnope.hotswap.model.filters.FilterGroup;
import lombok.Getter;
import lombok.ToString;
import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static io.gitlab.lulnope.hotswap.Strings.isNullOrEmpty;
import static java.util.stream.Collectors.toList;

/**
 * Specifies a path to be scanned for maven projects. All directories will be scanned
 * up to {@link #searchDepth} directories deep.
 *
 * @since 0.0.1
 */
@ToString
public class Workspace {

    /**
     * Root of workspace
     */
    @Parameter(required = true)
    private File path;
    /**
     * References to filters. Must contain valid id of filter group.
     *
     * @see FilterGroup
     */
    @Getter
    @Parameter
    private List<String> filterGroupIds = new ArrayList<>();

    /**
     * Path relative to every encountered ${project.basedir} on this workspace.
     * All projects encountered in this workspace will be assumed to
     * have their ${project.build.outputDirectory} pointing to this subdirectory.
     */
    @Parameter(defaultValue = "target/classes")
    private String buildSubdirectory = "target/classes";

    /**
     * Path relative to every encountered ${project.basedir} on this workspace.
     * All projects encountered in this workspace will be assumed to
     * have their resources stored in this subdirectory.
     */
    @Parameter(defaultValue = "src/main/resources")
    private String resourcesSubdirectory = "src/main/resources";

    /**
     * Path relative to every encountered ${project.basedir} on this workspace.
     * All projects encountered in this workspace will be assumed to
     * have their webapp dirs in this subdirectory.
     */
    @Parameter(defaultValue = "src/main/webapp")
    private String webappSubdirectory = "src/main/webapp";

    /**
     * Search depth of this workspace. How many directories deep should be scanned.
     */
    @Parameter(defaultValue = "3")
    private int searchDepth = 3;

    /**
     * If set to true - allows projects not specified in dependencies.
     */
    @Parameter(defaultValue = "false")
    @Getter
    private boolean allowUnrelatedProjects = false;

    private Path resolveResources(Path p) {
        return p.resolve(resourcesSubdirectory);
    }

    private Path resolveBuildOutput(Path p) {
        return p.resolve(buildSubdirectory);
    }

    private Path resolveWebappDir(Path p) {
        return p.resolve(webappSubdirectory);
    }

    private boolean exists() {
        return pathIsSet() && path.exists();
    }

    private boolean isDirectory() {
        return pathIsSet() && path.isDirectory();
    }

    private Path getAbsolutePath() {
        return Paths.get(path.getAbsolutePath());
    }

    private boolean pathIsSet() {
        return path != null;
    }

    public void validate() throws MojoFailureException {
        if (!pathIsSet()) {
            throw new MojoFailureException("Encoutered a workspace with no path set. Set path for all listed workspaces.");
        }

        if (!exists()) {
            throw new MojoFailureException("Workspace references non-existent path: " + getAbsolutePath());
        }

        if (!isDirectory()) {
            throw new MojoFailureException("Workspace path is not a directory: " + getAbsolutePath());
        }

        if (isNullOrEmpty(buildSubdirectory)) {
            throw new MojoFailureException("Build subdirectory is not set: " + getAbsolutePath());
        }

        if (isNullOrEmpty(resourcesSubdirectory)) {
            throw new MojoFailureException("Resources subdirectory is not set: " + getAbsolutePath());
        }

        if (searchDepth < 0) {
            throw new MojoFailureException("Search depth is < 0: " + getAbsolutePath());
        }
    }

    private List<Path> collectPomFiles() throws MojoFailureException {
        try (Stream<Path> paths = Files.walk(getAbsolutePath(), searchDepth)) {
            return paths
                    .filter(Files::isRegularFile)
                    .filter(e -> e.getFileName().toString().equals("pom.xml"))
                    .collect(toList());
        } catch (IOException e) {
            throw new MojoFailureException("Failed to collect pom.xml files from workspace path: " + path, e);
        }
    }

    public List<WorkspaceEntry> getWorkspaceEntries() throws MojoFailureException {
        List<WorkspaceEntry> result = new ArrayList<>();

        MavenXpp3Reader pomReader = new MavenXpp3Reader();
        List<Path> pomFiles = collectPomFiles();
        for (Path pomFile : pomFiles) {
            try {
                Model model = pomReader.read(new FileReader(pomFile.toFile()));

                Path projectRoot = pomFile.getParent();
                Path buildOutputRoot = resolveBuildOutput(projectRoot);
                Path resourcesRoot = resolveResources(projectRoot);
                Path webappRoot = resolveWebappDir(projectRoot);

                WorkspaceEntry entry = WorkspaceEntry.builder()
                        .model(model)
                        .buildOutputRoot(buildOutputRoot)
                        .resourcesRoot(resourcesRoot)
                        .webappRoot(webappRoot)
                        .projectRoot(projectRoot)
                        .filterGroupIds(filterGroupIds)
                        .allowUnrelated(allowUnrelatedProjects)
                        .build();

                result.add(entry);
            } catch (Exception e) {
                throw new MojoFailureException("Failed to read pom.xml file: " + pomFile, e);
            }
        }

        return result;
    }

}
