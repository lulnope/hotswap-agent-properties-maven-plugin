package io.gitlab.lulnope.hotswap.model.filters;

import java.util.ArrayList;
import java.util.List;

import org.apache.maven.plugins.annotations.Parameter;

import io.gitlab.lulnope.hotswap.model.Workspace;
import lombok.Getter;
import lombok.ToString;

/**
 * Collection of filters to be applied to a workspace identifiable by an {@link #id}
 * 
 * @see Workspace
 * @since 0.0.1
 */
@Getter
@ToString
public class FilterGroup {
	/**
	 * Id of this filter group.
	 * 
	 * @see Workspace#filterGroupIds
	 */
	@Parameter(required=true)
	private String id;
	
	/**
	 * Actual collection of filter
	 */
	@Parameter(required=true)
	private List<AbstractFilter> filters = new ArrayList<>();
}
