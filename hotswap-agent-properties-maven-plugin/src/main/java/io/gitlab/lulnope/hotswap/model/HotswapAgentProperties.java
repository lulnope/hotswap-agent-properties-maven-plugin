package io.gitlab.lulnope.hotswap.model;

import lombok.Builder;
import lombok.NonNull;

import java.nio.file.Path;
import java.util.Collection;
import java.util.List;

import static io.gitlab.lulnope.hotswap.Strings.isNeitherNullNorEmpty;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

@Builder
public class HotswapAgentProperties {

    @NonNull
    private final Collection<Path> extraClasspath;
    @NonNull
    private final Collection<Path> watchResources;
    @NonNull
    private final Collection<Path> webappDir;
    private final String disabledPlugins;
    private final boolean autoHotswap;
    private String springBasePackagePrefix;
    private Integer autoHotswapPort;
    private boolean osgiEquinoxDebugMode;
    @NonNull
    private WeldBeanReloadStrategy weldBeanReloadStrategy;
    @NonNull
    private final LoggerLevel logger;
    private final String loggerDateTimeFormat;
    private final String excludedClassLoaderPatterns;

    private String writeMultiline(String parameter, Collection<Path> values) {
        if (values.isEmpty()) {
            return "";
        }


        List<String> valuesAsStrings = values.stream()
                .map(Object::toString)
                .map(e -> e.replace('\\', '/'))
                .collect(toList());

        if (valuesAsStrings.size() == 1) {
            return parameter + "=" + valuesAsStrings.get(0) + "\n";
        }


        String multilineValue = valuesAsStrings.stream()
                .collect(joining("; \\\n "));

        return parameter + "=" + multilineValue + "\n";
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append(writeMultiline("extraClasspath", extraClasspath));
        builder.append(writeMultiline("watchResources", watchResources));
        builder.append(writeMultiline("webappDir", webappDir));
        if (isNeitherNullNorEmpty(disabledPlugins)) {
            builder.append("disabledPlugins=" + disabledPlugins + "\n");
        }
        builder.append("autoHotswap=" + autoHotswap + "\n");
        if (isNeitherNullNorEmpty(springBasePackagePrefix)) {
            builder.append("springBasePackagePrefix=" + springBasePackagePrefix + "\n");
        }
        if (autoHotswapPort != null) {
            builder.append("autoHotswap.port=" + autoHotswapPort + "\n");
        }
        builder.append("osgiEquinox.debugMode=" + osgiEquinoxDebugMode + "\n");
        builder.append("weld.beanReloadStrategy=" + weldBeanReloadStrategy + "\n");
        builder.append("LOGGER=" + logger + "\n");
        builder.append("LOGGER_DATETIME_FORMAT=" + loggerDateTimeFormat + "\n");
        if (isNeitherNullNorEmpty(excludedClassLoaderPatterns)) {
            builder.append("excludedClassLoaderPatterns=" + excludedClassLoaderPatterns + "\n");
        }

        return builder.toString();
    }
}
