package io.gitlab.lulnope.hotswap.model.filters;

import io.gitlab.lulnope.hotswap.model.WorkspaceEntry;
import lombok.ToString;

@ToString(callSuper=true)
public class GroupIdFilter extends AbstractProjectFilter {

	@Override
	String extractValue(WorkspaceEntry e) {
		return e.getId().getGroupId();
	}

}
