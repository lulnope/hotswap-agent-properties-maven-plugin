package io.gitlab.lulnope.hotswap.model;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.Value;
import org.apache.maven.model.Model;

import java.io.File;
import java.nio.file.Path;
import java.util.Collection;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Value
public class WorkspaceEntry {
    @EqualsAndHashCode.Include
    DependencyId id;
    Model model;
    Path buildOutputRoot;
    Path resourcesRoot;
    Path webappRoot;
    Path projectRoot;
    Collection<String> filterGroupIds;
    boolean allowUnrelated;


    @Builder
    private WorkspaceEntry(
            @NonNull Model model,
            @NonNull Path buildOutputRoot,
            @NonNull Path resourcesRoot,
            @NonNull Path webappRoot,
            @NonNull Path projectRoot,
            @NonNull Collection<String> filterGroupIds,
            boolean allowUnrelated
    ) {
        this.id = new DependencyId(model);
        this.model = model;
        this.buildOutputRoot = buildOutputRoot;
        this.resourcesRoot = resourcesRoot;
        this.webappRoot = webappRoot;
        this.projectRoot = projectRoot;
        this.filterGroupIds = filterGroupIds;
        this.allowUnrelated = allowUnrelated;
    }

    public List<DependencyId> getDependencyIds() {
        return model.getDependencies().stream()
                .map(DependencyId::new)
                .collect(toList());
    }

    public boolean buildOutputDirectoryExists() {
        File f = buildOutputRoot.toFile();
        return f.exists() && f.isDirectory();
    }

    public boolean resourcesDirectoryExists() {
        File f = resourcesRoot.toFile();
        return f.exists() && f.isDirectory();
    }

    public boolean webappDirectoryExists() {
        File f = webappRoot.toFile();
        return f.exists() && f.isDirectory();
    }
}
