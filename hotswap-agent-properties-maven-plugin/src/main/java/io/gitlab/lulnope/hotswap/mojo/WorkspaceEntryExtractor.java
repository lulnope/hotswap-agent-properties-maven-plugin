package io.gitlab.lulnope.hotswap.mojo;

import io.gitlab.lulnope.hotswap.model.DependencyId;
import io.gitlab.lulnope.hotswap.model.Workspace;
import io.gitlab.lulnope.hotswap.model.WorkspaceEntry;
import lombok.NonNull;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.shared.dependency.graph.DependencyNode;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

public class WorkspaceEntryExtractor {
    private final Map<DependencyId, WorkspaceEntry> workspaceEntriesById;

    public WorkspaceEntryExtractor(@NonNull Collection<Workspace> workspaces) throws MojoFailureException {
        this.workspaceEntriesById = getWorkspaceEntriesById(workspaces);
    }

    private Map<DependencyId, WorkspaceEntry> getWorkspaceEntriesById(Collection<Workspace> workspaces) throws MojoFailureException {
        Map<DependencyId, WorkspaceEntry> workspaceEntriesById = new HashMap<>();
        for (Workspace w : workspaces) {
            w.getWorkspaceEntries().forEach(e -> workspaceEntriesById.put(e.getId(), e));
        }

        return workspaceEntriesById;
    }

    public List<WorkspaceEntry> findMatchingWorkspaceEntries(@NonNull List<DependencyNode> dependencies) {
        String arts = dependencies.stream()
                .map(e -> e.getArtifact().toString())
                .collect(Collectors.joining("\n"));

        return dependencies.stream()
                .map(DependencyNode::getArtifact)
                .map(DependencyId::new)
                .map(workspaceEntriesById::get)
                .filter(Objects::nonNull)
                .collect(toList());
    }
}
