package io.gitlab.lulnope.hotswap.model.filters;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.maven.plugins.annotations.Parameter;

import io.gitlab.lulnope.hotswap.model.WorkspaceEntry;

public abstract class NestedFilter extends AbstractFilter {
	@Parameter(required=true)
	private List<AbstractFilter> filters = new ArrayList<>();
	
	protected abstract boolean test(Collection<AbstractFilter> filters, WorkspaceEntry e); 
	
	@Override
	public final boolean test(WorkspaceEntry e) {
		return test(Collections.unmodifiableList(filters), e);
	}
}
