package io.gitlab.lulnope.hotswap.model.filters;

import org.apache.maven.plugins.annotations.Parameter;

import io.gitlab.lulnope.hotswap.model.WorkspaceEntry;
import lombok.ToString;

@ToString(callSuper=true)
public abstract class AbstractProjectFilter extends AbstractFilter {

	/**
	 * Allowed values:
	 * <li>{@link #EQUALS}</li>
	 * <li>{@link #STARTS_WITH}</li>
	 * <li>{@link #ENDS_WITH}</li>
	 * <li>{@link #CONTAINS}</li>
	 * <li>{@link #REGEXP}</li>
	 * 
	 * @see Comparison
	 * @since 0.0.1
	 */
	@Parameter(required = true, defaultValue = "EQUALS")
	private Comparison comparison = Comparison.EQUALS;
	
	/**
	 * Value that will be compared according to comparison rule {@link #comparison} 
	 * with corresponding value in every encountered project.
	 * @since 0.0.1
	 */
	@Parameter(required = true)
	private String value;
	
	/**
	 * If set to true - all {@link comparison} operations executed by this filter
	 * will ignore case of both provided filter value and value read from encountered project.
	 * </br>
	 * If set to false all {@link comparison} operations executed by this filter 
	 * will be case sensitive.
	 * @since 0.0.1
	 */
	@Parameter(defaultValue = "true")
	private boolean ignoreCase = true;

	abstract String extractValue(WorkspaceEntry e);

	public boolean test(WorkspaceEntry e) {
		String extractedValue = extractValue(e);
		if (extractedValue == null) {
			return false;
		}

		if (ignoreCase) {
			value = value.toLowerCase();
			extractedValue = extractedValue.toLowerCase();
		}

		if (isNegate()) {
			return comparison.negate().test(value, extractedValue);
		} else {
			return comparison.test(value, extractedValue);
		}
	}
}
