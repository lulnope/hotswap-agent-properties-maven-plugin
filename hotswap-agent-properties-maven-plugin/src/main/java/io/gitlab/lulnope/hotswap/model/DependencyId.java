package io.gitlab.lulnope.hotswap.model;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.model.Dependency;
import org.apache.maven.model.Model;
import org.apache.maven.model.Parent;

import io.gitlab.lulnope.hotswap.Strings;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor(access=AccessLevel.PACKAGE)
public class DependencyId {
	String artifactId;
	String groupId;
	
	
	public DependencyId(Dependency dependency) {
		this(dependency.getArtifactId(), dependency.getGroupId());
	}
	
	DependencyId(Model model) {
		this(model.getArtifactId(), extractGroupId(model));
	}
	
	
	public DependencyId(Artifact artifact) {
		this(artifact.getArtifactId(), artifact.getGroupId());
	}
	
	
	private static String extractGroupId(Model model) {
		String groupId = model.getGroupId();
		if(Strings.isNeitherNullNorEmpty(groupId)) {
			return groupId;
		}
		
		Parent parent = model.getParent();
		if(parent == null) {
			return null;
		}
		
		String parentGroupId = parent.getGroupId();
		if(Strings.isNeitherNullNorEmpty(parentGroupId)) {
			return parentGroupId;
		}
		
		return null;
	}
}
